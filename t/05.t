use v6.c;
use Test;
use lib 'lib';

constant test_plan = 24;
use Net::Ethereum;

plan 2;

my $unlockpassw   = @*ARGS[0] // 'node0';

my $marshal_test1 = '0x70381af8000000000000000000000000000000000000000000000000000000000000000200000000000000000000000000000000000000000000000000000000000000e0000000000000000000000000000000000000000000000000000000000000012000000000000000000000000000000000000000000000000000000000000001600000000000000000000000000000000000000000000000000000000000000220000000000000000000000000000000000000000000000000000000000000026000000000000000000000000000000000000000000000000000000000000002a00000000000000000000000000000000000000000000000000000000000000016687474703a2f2f7065726c362e70686569782e6f726700000000000000000000000000000000000000000000000000000000000000000000000000000000000f3235352e3235352e3235352e313031000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000984d6f7a696c6c612f352e3020286950686f6e653b20435055206950686f6e65204f5320375f30206c696b65204d6163204f53205829204170706c655765624b69742f3533372e35312e3120284b48544d4c2c206c696b65204765636b6f292056657273696f6e2f372e30204d6f62696c652f313141343635205361666172692f393533372e35332042696e67507265766965772f312e306200000000000000000000000000000000000000000000000000000000000000000000000000000009313930302a313238300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000e2f666565646261636b2e68746d6c00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000025553000000000000000000000000000000000000000000000000000000000000';

my $marshal_test2 = '0x7f265ec6000000000000000000000000000000000000000000000000000000000000037b00000000000000000000000000000000000000000000000000000000000000e00000000000000000000000000000000000000000000000000000000000000120000000000000000000000000000000000000000000000000000000000000016000000000000000000000000000000000000000000000000000000000000001e0000000000000000000000000000000000000000000000000000000000000022000000000000000000000000000000000000000000000000000000000000002600000000000000000000000000000000000000000000000000000000000000010687474703a2f2f79616e6465782e727500000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000e37302e3131382e3132302e313036000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004c4d6f7a696c6c612f352e3020285831313b205562756e74753b204c696e7578207838365f36343b2072763a36302e3029204765636b6f2f32303130303130312046697265666f782f36302e3000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000009313230302a313630300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000b2f696e6465782e68746d6c00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000024649000000000000000000000000000000000000000000000000000000000000';

my @test_hashes = [
    {
        id         => 1,
        referer    => 'goo.gl',
        ip         => '127.0.0.1',
        useragent  => 'Mozilla',
        resolution => '640*480',
        page       => 'index.html',
        country    => 'RU'
    },
    {
        id         => 2,
        referer    => 'twitter.com',
        ip         => '127.0.0.11',
        useragent  => 'IE',
        resolution => '1900*1280',
        page       => 'index2.html',
        country    => 'US'
    },
    {
        id         => 3,
        referer    => 'ya.ru',
        ip         => '127.0.0.12',
        useragent  => 'Opera',
        resolution => '1024*768',
        page       => 'index3.html',
        country    => 'BY'
    },
    {
        id         => 4,
        referer    => 'pheix.org',
        ip         => '127.0.0.111',
        useragent  => 'Safari',
        resolution => '100*200',
        page       => 'index4.html',
        country    => 'UA'
    },
    {
        id         => 5,
        referer    => 'foo.bar',
        ip         => '127.0.0.254',
        useragent  => 'Netscape',
        resolution => '1152*778',
        page       => 'index5.html',
        country    => 'RO'
    },
];

my %f_params1 =
    _id    => 2,
    _ref   => 'http://perl6.pheix.org',
    _ip    => '255.255.255.101',
    _ua    => 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) ' ~
              'AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 ' ~
              'Mobile/11A465 Safari/9537.53 BingPreview/1.0b',
    _res   => '1900*1280',
    _pg    => '/feedback.html',
    _cntry => 'US',
;

my %f_params2 =
    _id    => 891,
    _ref   => 'http://yandex.ru',
    _ip    => '70.118.120.106',
    _ua    => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:60.0) ' ~
              'Gecko/20100101 Firefox/60.0',
    _res   => '1200*1600',
    _pg    => '/index.html',
    _cntry => 'FI',
;

my Str  $contract = ( '0x' ~ '0' x 40 );
my Str  $transact = %*ENV<TXHASH> // ( '0x' ~ '0' x 64 );

#close connection subtest
subtest {
    plan test_plan;
    my $obj = Net::Ethereum.new(
        api_url => 'http://127.0.0.1:8501',
        show_progress => False,
        unlockpwd     => $unlockpassw,
    );
    my %h = $obj.node_ping;
    if ( %h<retcode> == -1 ) {
        skip-rest('ethereum node is down');
    }
    else {
        $obj.tx_wait_sec = 1;
        is $obj.personal_unlockAccount, True, 'account is unlocked';
        $obj.abi = './t/abi/test.abi'.IO.slurp.Str;

        my Int  $tsts  = 1;
        my      %params;
        my      %tst_h;
        my      @tst_a;
        my Str  $tst_s;
        my Int  $tst_i;
        my Bool $tst_b;
        my Real $tst_r;

        if $transact !~~ m:i/^ <[0x]>+ $/ {
            ok(
                ($transact ~~ m:i/^ 0x<[0..9a..f]>**64 $/ ),
                'valid transaction hash ' ~ $transact
            );
            %tst_h = $obj.eth_getTransactionReceipt( $transact );
            ok(
                %tst_h,
                'eth_getTransactionReceipt from ' ~
                ( %tst_h<from> || 'undefined' )   ~
                ' (status: ' ~ ( %tst_h<status> || '-1' ) ~
                ', used gas: ' ~ :16( %tst_h<gasUsed> || '0x0' ) ~ q{)},
            );
            if :16(%tst_h<status>) == 1 {
                $contract = $obj.retrieve_contract( $transact );
                ok(
                    ($contract ~~ m:i/^ 0x<[0..9a..f]>**40 $/ ),
                    'valid smart contract addr ' ~ $contract
                );
            }
            else {
                skip 'tx ' ~ $transact  ~ ' not mined', 1;
            }
        } else {
            skip 'no valid hash for eth_getTransactionByHash is passed through input args', 3;
        }

        $tst_s = $obj.marshal( 'insert', %f_params1 );
        is $tst_s.lc, $marshal_test1.lc, 'marshal for insert()';

        $tst_s = $obj.marshal( 'set_by_id', %f_params2 );
        is $tst_s.lc, $marshal_test2.lc, 'marshal for set_by_id()';

        if $contract !~~ m:i/^ <[0x]>+ $/ {
            $tst_s = $obj.sendTransaction(
                $obj.eth_accounts[0],
                $contract,
                'insert',
                %f_params2,
                4700000,
            );
            ok(
                $tst_s ~~ m:i/^ 0x<[0..9a..f]>* $/,
                'sendTransaction insert hash=' ~ $tst_s,
            );
            $obj.wait_for_transaction( :hashes( @($tst_s) ) );
        } else {
            skip 'no valid smart contract hash is defined', 1;
        }

        $tst_i = $obj.deploy_contract_estimate_gas(
            $obj.eth_accounts[0],
            '0x' ~ './t/abi/test.bin'.IO.slurp.Str, %( Nil )
        );
        ok $tst_i > 0, 'deploy_contract_estimate_gas gas=' ~ $tst_i;
        is(
            $obj.contract_id.defined, False,
            'attr $.contract_id is not defined',
        );

        $tst_s = $obj.deploy_contract(
            $obj.eth_accounts[0],
            '0x' ~ './t/abi/test.bin'.IO.slurp.Str, %( Nil ),
            $tst_i
        );
        ok(
            $tst_s ~~ m:i/^ 0x<[0..9a..f]>* $/,
            'deploy_contract transaction hash=' ~ $tst_s
        );
        $obj.wait_for_transaction(
            hashes   => @($tst_s),
            iters    => 15,
            contract => True,
        );

        is(
            $obj.contract_id.defined, True,
            'attr $.contract_id is defined: ' ~ $obj.contract_id,
        );

        $tst_i =
            $obj.contract_method_call_estimate_gas('insert', %f_params1);
        ok(
            $tst_i > 0,
            'contract_method_call_estimate_gas insert gas=' ~ $tst_i,
        );

        $tst_i =
            $obj.contract_method_call_estimate_gas('set_by_id', %f_params2 );
        ok(
            $tst_i > 0,
            'contract_method_call_estimate_gas set_by_id gas=' ~ $tst_i,
        );

        $tst_i =
            $obj.contract_method_call_estimate_gas('init_db', %( Nil ) );
        ok(
            $tst_i > 0,
            'contract_method_call_estimate_gas int_db gas=' ~ $tst_i,
        );

        $tst_i = $obj.contract_method_call_estimate_gas(
            'remove_by_id',
            %( _id => 1 )
        );
        ok(
            $tst_i > 0,
            'contract_method_call_estimate_gas remove_by_id gas=' ~ $tst_i,
        );

        $tst_i = $obj.contract_method_call_estimate_gas('get_count', %( Nil ) );
        ok(
            $tst_i > 0,
            'contract_method_call_estimate_gas get_count gas=' ~ $tst_i,
        );

        $tst_i =
            $obj.contract_method_call_estimate_gas('get_max_id', %( Nil ) );
        ok(
            $tst_i > 0,
            'contract_method_call_estimate_gas get_max_id gas=' ~ $tst_i,
        );

        $tst_s = $obj.sendTransaction(
            $obj.eth_accounts[0],
            $obj.contract_id,
            'init_db',
            %( Nil ),
            4700000,
        );
        ok(
            $tst_s ~~ m:i/^ 0x<[0..9a..f]>* $/,
            'sendTransaction init_db hash=' ~ $tst_s,
        );
        $obj.wait_for_transaction( :hashes( @($tst_s) ) );

        for (@test_hashes) -> %hash {
            %tst_h = $obj.contract_method_call(
                'get_by_id',
                %( _id => %hash<id>)
            );
            is-deeply(
                %tst_h,
                %hash,
                'contract_method_call for get_by_id done with id=' ~ %hash<id>,
            );
        };
        if !($obj.keepalive) {
            is(
                $obj.finalize_request, False,
                'close connection on not keep-alive'
            );
        } else {
            skip 'close connection', 1;
        }
    }
}, 'close connection subtest';

# keep-alive connection subtest
subtest {
    plan test_plan;
    my $obj = Net::Ethereum.new(
        api_url => 'http://127.0.0.1:8501',
        show_progress => False,
        unlockpwd     => $unlockpassw,
        keepalive     => True,
    );
    my %h = $obj.node_ping;
    if ( %h<retcode> == -1 ) {
        skip-rest('ethereum node is down');
    }
    else {
        if !($obj.check_ua_keepalive) {
            skip-rest('user-agent not supporting keep-alive');
        } else {
            $obj.tx_wait_sec = 1;
            is $obj.personal_unlockAccount, True, 'account is unlocked';
            $obj.abi = './t/abi/test.abi'.IO.slurp.Str;

            my Int  $tsts  = 1;
            my      %params;
            my      %tst_h;
            my      @tst_a;
            my Str  $tst_s;
            my Int  $tst_i;
            my Bool $tst_b;
            my Real $tst_r;

            if $transact !~~ m:i/^ <[0x]>+ $/ {
                ok(
                    ($transact ~~ m:i/^ 0x<[0..9a..f]>**64 $/ ),
                    'valid transaction hash ' ~ $transact
                );
                %tst_h = $obj.eth_getTransactionReceipt( $transact );
                ok(
                    %tst_h,
                    'eth_getTransactionReceipt from ' ~
                    ( %tst_h<from> || 'undefined' )   ~
                    ' (status: ' ~ ( %tst_h<status> || '-1' ) ~
                    ', used gas: ' ~ :16( %tst_h<gasUsed> || '0x0' ) ~ q{)},
                );
                if :16(%tst_h<status>) == 1 {
                    $contract = $obj.retrieve_contract( $transact );
                    ok(
                        ($contract ~~ m:i/^ 0x<[0..9a..f]>**40 $/ ),
                        'valid smart contract addr ' ~ $contract
                    );
                }
                else {
                    skip 'tx ' ~ $transact  ~ ' not mined', 1;
                }
            } else {
                skip 'no valid hash for eth_getTransactionByHash is passed through input args', 3;
            }

            $tst_s = $obj.marshal( 'insert', %f_params1 );
            is $tst_s.lc, $marshal_test1.lc, 'marshal for insert()';

            $tst_s = $obj.marshal( 'set_by_id', %f_params2 );
            is $tst_s.lc, $marshal_test2.lc, 'marshal for set_by_id()';

            if $contract !~~ m:i/^ <[0x]>+ $/ {
                $tst_s = $obj.sendTransaction(
                    $obj.eth_accounts[0],
                    $contract,
                    'insert',
                    %f_params2,
                    4700000,
                );
                ok(
                    $tst_s ~~ m:i/^ 0x<[0..9a..f]>* $/,
                    'sendTransaction insert hash=' ~ $tst_s,
                );
                $obj.wait_for_transaction( :hashes( @($tst_s) ) );
            } else {
                skip 'no valid smart contract hash is defined', 1;
            }

            $tst_i = $obj.deploy_contract_estimate_gas(
                $obj.eth_accounts[0],
                '0x' ~ './t/abi/test.bin'.IO.slurp.Str, %( Nil )
            );
            ok $tst_i > 0, 'deploy_contract_estimate_gas gas=' ~ $tst_i;
            is(
                $obj.contract_id.defined, False,
                'attr $.contract_id is not defined',
            );

            $tst_s = $obj.deploy_contract(
                $obj.eth_accounts[0],
                '0x' ~ './t/abi/test.bin'.IO.slurp.Str, %( Nil ),
                $tst_i
            );
            ok(
                $tst_s ~~ m:i/^ 0x<[0..9a..f]>* $/,
                'deploy_contract transaction hash=' ~ $tst_s
            );
            $obj.wait_for_transaction(
                hashes   => @($tst_s),
                iters    => 15,
                contract => True,
            );

            is(
                $obj.contract_id.defined, True,
                'attr $.contract_id is defined: ' ~ $obj.contract_id,
            );

            $tst_i =
                $obj.contract_method_call_estimate_gas('insert', %f_params1);
            ok(
                $tst_i > 0,
                'contract_method_call_estimate_gas insert gas=' ~ $tst_i,
            );

            $tst_i = $obj.contract_method_call_estimate_gas(
                'set_by_id',
                %f_params2
            );
            ok(
                $tst_i > 0,
                'contract_method_call_estimate_gas set_by_id gas=' ~ $tst_i,
            );

            $tst_i =
                $obj.contract_method_call_estimate_gas('init_db', %( Nil ) );
            ok(
                $tst_i > 0,
                'contract_method_call_estimate_gas int_db gas=' ~ $tst_i,
            );

            $tst_i = $obj.contract_method_call_estimate_gas(
                'remove_by_id',
                %( _id => 1 )
            );
            ok(
                $tst_i > 0,
                'contract_method_call_estimate_gas remove_by_id gas=' ~ $tst_i,
            );

            $tst_i =
                $obj.contract_method_call_estimate_gas('get_count', %( Nil ) );
            ok(
                $tst_i > 0,
                'contract_method_call_estimate_gas get_count gas=' ~ $tst_i,
            );

            $tst_i =
                $obj.contract_method_call_estimate_gas('get_max_id', %( Nil ) );
            ok(
                $tst_i > 0,
                'contract_method_call_estimate_gas get_max_id gas=' ~ $tst_i,
            );

            $tst_s = $obj.sendTransaction(
                $obj.eth_accounts[0],
                $obj.contract_id,
                'init_db',
                %( Nil ),
                4700000,
            );
            ok(
                $tst_s ~~ m:i/^ 0x<[0..9a..f]>* $/,
                'sendTransaction init_db hash=' ~ $tst_s,
            );
            $obj.wait_for_transaction( :hashes( @($tst_s) ) );

            for (@test_hashes) -> %hash {
                %tst_h = $obj.contract_method_call(
                    'get_by_id',
                    %( _id => %hash<id>)
                );
                is-deeply(
                    %tst_h,
                    %hash,
                    'contract_method_call for get_by_id done ' ~
                    ' with id=' ~ %hash<id>,
                );
            };
            if $obj.keepalive {
                is $obj.finalize_request, True, 'close connection';
            } else {
                skip 'close connection', 1;
            }
        }
    }
}, 'keep-alive connection subtest';

done-testing;
