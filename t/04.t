use v6.c;
use Test;
use lib 'lib';

use Net::Ethereum;
my $obj = Net::Ethereum.new(api_url => 'http://127.0.0.1:8501');

my Int  $tsts  = 9999;
my      %params;
my      %tst_h;
my      @tst_a;
my Str  $tst_s;
my Int  $tst_i;
my Bool $tst_b;
my Real $tst_r;
my Str  $contract = ( '0x' ~ '0' x 40 );
my Str  $transact = %*ENV<TXHASH> // ( '0x' ~ '0' x 64 );

subtest {
    plan 10000;
    for (0..$tsts) {
        my Int $integer = (-10000000..10000000).rand.Int;
        my Str $_m  = $obj.marshal_int( $integer );
        my Int $_um = $obj.unmarshal_int( $_m );
        is $integer, $_um, 'marshal/unmarshal for ' ~ $integer;
    }
}, 'marshal/unmarshal subtest';

%tst_h = $obj.node_ping;
if ( %tst_h<retcode> == -1 ) {
    skip 'ethereum node is down', 37;
    # ( '***INFO: ethereum node is down, skipping ' ~ 37 ~ ' tests from ' ~ $*PROGRAM-NAME ).say;
    # done-testing;
    # exit 0;
}
else {
    subtest {
        plan 40;
        if $transact !~~ m:i/^ <[0x]>+ $/ {
            ok(
                ($transact ~~ m:i/^ 0x<[0..9a..f]>**64 $/ ),
                'valid transaction hash ' ~ $transact
            );
            %tst_h = $obj.eth_getTransactionReceipt( $transact );
            ok(
                %tst_h,
                'eth_getTransactionReceipt from ' ~
                ( %tst_h<from> || 'undefined' )   ~
                ' (status: ' ~ ( %tst_h<status> || '-1' ) ~
                ', used gas: ' ~ :16( %tst_h<gasUsed> || '0x0' ) ~ q{)},
            );
            if :16(%tst_h<status>) == 1 {
                $contract = $obj.retrieve_contract( $transact );
                ok(
                    ($contract ~~ m:i/^ 0x<[0..9a..f]>**40 $/ ),
                    'valid smart contract addr ' ~ $contract
                );
            }
            else {
                skip 'tx ' ~ $transact  ~ ' not mined', 1;
            }
        } else {
            skip 'no valid hash for eth_getTransactionByHash is passed through input args', 3;
        }
        $tst_s = $obj.getContractMethodId('get_max_id()');
        is $tst_s, '0x30636152', 'getContractMethodId for get_max_id()';

        $tst_s = $obj.getContractMethodId('get_by_id(uint256)');
        is $tst_s, '0x173b3500', 'getContractMethodId for get_by_id(uint256)';

        $tst_s = $obj.getContractMethodId('get_count()');
        is $tst_s, '0xe7278e7f', 'getContractMethodId for get_count()';

        $tst_s = $obj.getContractMethodId('init_db()');
        is $tst_s, '0x00200170', 'getContractMethodId for init_db()';

        $tst_s = $obj.getContractMethodId('get_by_index(uint256)');
        is $tst_s, '0xea6201f1', 'getContractMethodId for get_by_index(uint256)';

        $tst_s = $obj.getContractMethodId('remove_by_id(uint256)');
        is $tst_s, '0x5ae9d5da', 'getContractMethodId for remove_by_id(uint256)';

        $obj = Net::Ethereum.new(api_url => 'http://127.0.0.1:8501');

        $obj.abi = './t/abi/test.abi'.IO.slurp.Str;
        is $obj.abi, './t/abi/test.abi'.IO.slurp.Str, 'abi check setter';

        %tst_h = $obj.get_function_abi('set_by_id');
        is %tst_h<name>, 'set_by_id', 'get_function_abi for ' ~ %tst_h<name> ~ ': inputs=' ~ %tst_h<inputs>.elems ~ ', outputs=' ~ %tst_h<outputs>.elems;

        %tst_h = $obj.get_constructor_abi;
        ok !%tst_h, 'no constructor in test abi';

        my @marshals = (
            %( int => 1000, marshal => '00000000000000000000000000000000000000000000000000000000000003e8' ),
            %( int => 32300, marshal => '0000000000000000000000000000000000000000000000000000000000007e2c' ),
            %( int => 0, marshal => '0000000000000000000000000000000000000000000000000000000000000000' ),
            %( int => -10, marshal => 'fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff6' ),
            %( int => -29937, marshal => 'ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8b0f' ),
            %( int => 1, marshal => '0000000000000000000000000000000000000000000000000000000000000001' ),
            %( int => -19929, marshal => 'ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffb227' ),
            %( int => 65535, marshal => '000000000000000000000000000000000000000000000000000000000000ffff' ),
            %( int => 9939, marshal => '00000000000000000000000000000000000000000000000000000000000026d3' ),
            %( int => -77399773993, marshal => 'ffffffffffffffffffffffffffffffffffffffffffffffffffffffedfa9d2cd7' ),
            %( int => -65565, marshal => 'fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffeffe3' ),
            %( int => 21365565, marshal => '000000000000000000000000000000000000000000000000000000000146033d' ),
            %( int => 565, marshal => '0000000000000000000000000000000000000000000000000000000000000235' ),
            %( int => 1982, marshal => '00000000000000000000000000000000000000000000000000000000000007be' ),
        );

        $obj = Net::Ethereum.new(api_url => 'http://127.0.0.1:8501');
        for @marshals -> %h {
            $tst_s = $obj.marshal_int( %h<int> );
            is $tst_s, %h<marshal>, 'marshal_int(' ~ %h<int> ~ ')';
            $tst_i = $obj.unmarshal_int( %h<marshal> );
            is $tst_i, %h<int>, 'unmarshal_int(' ~ %h<marshal> ~ ')';
        }
    }, 'getContractMethodId and deep marshalling subtest';
}

done-testing;
