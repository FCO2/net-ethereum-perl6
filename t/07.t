use Test;
use lib 'lib';

use Net::Ethereum;
my $obj = Net::Ethereum.new(
    api_url       => 'http://127.0.0.1:8501',
    show_progress => True,
    unlockpwd     => 'ospasswd',
);

my %thash_1;
my %thash_2;
my Str  $blck_str;
my Bool $raise_except;
my Int  $block;

plan 3;

if $obj {
    my %h = $obj.node_ping;
    if ( %h<retcode> > -1 ) {
        # testing eth_getBlockByNumber method
        subtest {
            plan 6;
            $blck_str = 'latest';
            %thash_1  = $obj.eth_getBlockByNumber( $blck_str );
            %thash_2  = $obj.eth_getBlockByNumber( $blck_str, True );
            ok(
                ( %thash_1 && %thash_2 ),
                'get data for ' ~ $blck_str ~ ' block: ' ~
                +(%thash_1<timestamp>) ~ q{/} ~ +(%thash_2<timestamp>)
            );
            $blck_str = 'earliest';
            %thash_1  = $obj.eth_getBlockByNumber( $blck_str );
            %thash_2  = $obj.eth_getBlockByNumber( $blck_str, True );
            ok(
                ( %thash_1 && %thash_2 ),
                'get data for ' ~ $blck_str ~ ' block: ' ~
                +(%thash_1<timestamp>) ~ q{/} ~ +(%thash_2<timestamp>)
            );
            $blck_str = 'pending';
            %thash_1  = $obj.eth_getBlockByNumber( $blck_str );
            %thash_2  = $obj.eth_getBlockByNumber( $blck_str, True );
            ok(
                ( %thash_1 && %thash_2 ),
                'get data for ' ~ $blck_str ~ ' block: ' ~
                +(%thash_1<timestamp>) ~ q{/} ~ +(%thash_2<timestamp>)
            );
            $raise_except = False;
            $block = -1;
            dies-ok {
                $obj.eth_getBlockByNumber( $block );
            }, 'raise exception if Int $block=' ~ $block;

            $block = 0;
            $raise_except = False;
            lives-ok {
                $obj.eth_getBlockByNumber( $block );
            }, 'no exception if Int $block=' ~ $block;

            $block = $obj.eth_blockNumber;
            if $block > 0 {
                %thash_1 = $obj.eth_getBlockByNumber( $block - 1 );
                ok(
                    %thash_1,
                    'get data for block no.' ~ ($block - 1) ~ ', timestamp=' ~
                    +(%thash_1<timestamp>)
                );
            } else {
                skip 'no prev blocks in chain: $block=' ~ $block, 1;
            }

        }, 'testing eth_getBlockByNumber method';

        # testing eth_getBlockByHash method
        subtest {
            plan 5;
            $block = $obj.eth_blockNumber;
            if $block >= 0 {
                %thash_1 = $obj.eth_getBlockByNumber( $block );
                ok(
                    %thash_1,
                    'get hash for block no.' ~ ($block - 1) ~ ' <' ~
                    %thash_1<hash> ~ q{>}
                );
                %thash_2 = $obj.eth_getBlockByHash( %thash_1<hash> );
                is-deeply(
                    %thash_1, %thash_2,
                    'block data is equal'
                );
                %thash_1 = $obj.eth_getBlockByNumber( $block, True );
                ok(
                    %thash_1,
                    'get hash for block no.' ~ ($block - 1) ~ ' <' ~
                    %thash_1<hash> ~ '> (True)'
                );
                %thash_2 = $obj.eth_getBlockByHash( %thash_1<hash>, True );
                is-deeply(
                    %thash_1, %thash_2,
                    'block data is equal (True)'
                );
                $raise_except = False;
                dies-ok {
                    $obj.eth_getBlockByHash( %thash_1<hash> ~ 'deadbeef' )
                }, 'raise exception on non-32 bytes hash'
            } else {
                skip 'no prev blocks in chain: $block=' ~ $block, 1;
            }
        }, 'testing eth_getBlockByHash method';

        # testing eth_getLogs method
        subtest {
            plan 3;
            my Str $ev = 'BigBro_access(bytes32,uint256)';
            my $tsha   = $obj.web3_sha3( $obj.string2hex( $ev ) );
            my %filter = $obj.pack_filter_params( topics => [$tsha] );
            my $logs   = $obj.eth_getLogs( %filter );
            todo ( 'possibly missed event: ' ~ $ev ), 1;
            ok( $logs.elems, 'get log records from blockchain' );
            if ($logs.elems) {
                my %logrec = $logs.tail;
                %thash_1   = $obj.eth_getBlockByHash( %logrec<blockHash> );
                ok(
                    %thash_1,
                    'get data for block <' ~ %logrec<blockHash> ~ '>'
                );
                my $lastmod = DateTime.new(+(%thash_1<timestamp>));
                ok(
                    $lastmod.defined,
                    'get lastmod blockchain time: ' ~ $lastmod
                );
            }
            else {
                skip 'no log records available', 2;
            }
        }, 'testing eth_getLogs method';
    }
    else {
        skip-rest('ethereum node is down');
    }
}
else {
    skip-rest('ethereum object is missed');
}

done-testing;
