use v6.c;
use Test;
use lib 'lib';

use Net::Ethereum;

my $unlockpassw   = @*ARGS[0] // 'node0';

my $obj = Net::Ethereum.new(
    api_url       => 'http://127.0.0.1:8501',
    show_progress => True,
    unlockpwd     => $unlockpassw,
);
my %pingh = $obj.node_ping;

if %pingh<retcode> == -1 {
    plan :skip-all<ethereum node is down>;
}
elsif !($obj.solidity.IO.e) {
    plan :skip-all<solidity is not installed>;
}
else {
    plan 19;

    my %tst_h;
    my @hashes;
    my Int $tst_i;
    my Str $tst_s;

    %tst_h = $obj.compile_and_deploy_contract(
        contract_src_path   => ( $*HOME.path ~ '/git/pheix-research/smart-contracts/sol/BigBro.sol' ),
        compile_output_path => './t/abi',
    );

    ok %tst_h, 'compile_and_deploy_contract returned Hash';
    ok %tst_h<blockHash> ~~ m:i/^ 0x<[0..9a..f]>* $/, 'compile_and_deploy_contract blockHash=' ~ %tst_h<blockHash>;
    ok %tst_h<blockNumber> ~~ m:i/^ 0x<[0..9a..f]>* $/, 'compile_and_deploy_contract blockNumber=' ~ %tst_h<blockNumber>;
    ok %tst_h<contractAddress> ~~ m:i/^ 0x<[0..9a..f]>* $/, 'compile_and_deploy_contract contractAddress=' ~ %tst_h<contractAddress>;
    ok %tst_h<from> eq $obj.eth_accounts[0], 'compile_and_deploy_contract from=' ~ %tst_h<from>;
    ok %tst_h<transactionHash> ~~ m:i/^ 0x<[0..9a..f]>* $/, 'compile_and_deploy_contract transactionHash=' ~ %tst_h<transactionHash>;
    ok %tst_h<status> == 1, 'compile_and_deploy_contract status=' ~ %tst_h<status>;
    ok !'./t/abi/BigBro.abi'.IO.e, 'compile_and_deploy_contract unlink *.abi';
    ok !'./t/abi/BigBro.bin'.IO.e, 'compile_and_deploy_contract unlink *.bin';
    $tst_s = $obj.retrieve_contract( %tst_h<transactionHash> );
    ok ( $tst_s eq %tst_h<contractAddress> ), 'retrieve_contract address=' ~ $tst_s;

    ( "\n" ~ %tst_h.gist ~ "\n").say;

    my %insert_hash =
        _id => 1982,
        _ref => 'perl6.pheix.org',
        _ip => '78.47.192.226',
        _ua => 'Netscape Navigator 4',
        _res => '1900*1200',
        _pg => 'hello-world.html',
        _cntry => 'DE'
    ;
    my %select_hash =
        id => 1982,
        referer => 'perl6.pheix.org',
        ip => '78.47.192.226',
        useragent => 'Netscape Navigator 4',
        resolution => '1900*1200',
        page => 'hello-world.html',
        country => 'DE'
    ;
    @hashes.push( $obj.sendTransaction( $obj.eth_accounts[0], %tst_h<contractAddress>, 'init_db', %( Nil ), 4700000) );
    ok @hashes.tail ~~ m:i/^ 0x<[0..9a..f]>* $/, 'sendTransaction init_db hash=' ~ @hashes.tail;
    for ( 1..5 ) {
        @hashes.push( $obj.sendTransaction( $obj.eth_accounts[0], %tst_h<contractAddress>, 'remove_by_id', %( _id => $_ ), 4700000) );
        ok @hashes.tail ~~ m:i/^ 0x<[0..9a..f]>* $/, 'sendTransaction remove_by_id hash=' ~ @hashes.tail;
    };
    @hashes.push( $obj.sendTransaction( $obj.eth_accounts[0], %tst_h<contractAddress>, 'insert', %insert_hash, 4700000) );
    ok @hashes.tail ~~ m:i/^ 0x<[0..9a..f]>* $/, 'sendTransaction insert hash=' ~ @hashes.tail;

    ( "***INFO: wait hashes array len: " ~ @hashes.elems ).say;

    $obj.wait_for_transaction( :hashes( @hashes ) );
    %tst_h = $obj.contract_method_call('get_count', %( Nil ) );
    ok %tst_h<count> == 1, 'contract_method_call get_count returns ' ~ %tst_h<count>;
    %tst_h = $obj.contract_method_call('get_by_id', %( _id => %select_hash<id>) );
    is-deeply %tst_h, %select_hash, 'contract_method_call for get_by_id done with id=' ~ %select_hash<id>;
}

done-testing;
