# Usage:   perl6 ./t/02.t <contract_addr> <transaction_hash>
# example: perl6 ./t/02.t 0xfd5d8108b0a9ffdb9e98e1019e176fd9e225a572 0xb57c357706c10249751d1062bdd90f02645564a2cf7759e8b38679d45dc5d70d

use v6.c;
use Test;
use lib 'lib';

constant test_plan = 33;
use Net::Ethereum;
my $obj = Net::Ethereum.new(
    api_url       => 'http://127.0.0.1:8501',
    show_progress => True,
    unlockpwd     => 'ospasswd',
);

my %h = $obj.node_ping;
if ( %h<retcode> == -1 ) {
    plan :skip-all<ethereum node is down>;
}
else {
    plan test_plan;

    my      %params;
    my      %tst_h;
    my      @tst_a;
    my Str  $tst_s;
    my Int  $tst_i;
    my Bool $tst_b;
    my Real $tst_r;
    my Int  $block;
    my Str  $tx = %*ENV<TXHASH> // ( '0x' ~ '0' x 64 );

    my Str $contract = ( '0x' ~ '0' x 40 );
    my Str $transact = @*ARGS[0] // $tx;
    my Str $accnt    = $obj.eth_accounts[0];

    if ( $obj.web3_clientVersion ~~ m:i/ 'parity-ethereum' /  ) {
        $tst_b = $obj.personal_unlockAccount( password => 'node0' );
        ok $tst_b, 'personal_unlockAccount on Parity node: ' ~ $tst_b;
    } else {
        $tst_b = $obj.personal_unlockAccount;
        ok $tst_b, 'personal_unlockAccount on non-Parity node: ' ~ $tst_b;
    }

    $block = $obj.eth_blockNumber;
    if $block > 0 {
        my Str $_hex = '0x' ~ $block.base(16).Str;
        $tst_i = $obj.eth_getBalance($accnt, $_hex);
        ok ( $tst_i >= 0 ), 'eth_getBalance at block ' ~ $_hex ~ ': ' ~ $tst_i;
    }
    else {
        skip 'eth_getBalance: no blocks in chain', 1;
    }

    $tst_i = $obj.eth_getBalance($accnt, 'latest');
    ok ( $tst_i >= 0 ), 'eth_getBalance latest: ' ~ $tst_i;

    $tst_i = $obj.eth_getBalance($accnt, 'earliest');
    ok ( $tst_i >= 0 ), 'eth_getBalance earliest: ' ~ $tst_i;

    $tst_i = $obj.eth_getBalance($accnt, 'pending');
    ok ( $tst_i >= 0 ), 'eth_getBalance pending: ' ~ $tst_i;

    $block = $obj.eth_blockNumber;
    if $block > 0 {
        my Str $_hex = '0x' ~ $block.base(16).Str;
        $tst_i = $obj.eth_getTransactionCount($accnt, $_hex);
        ok(
            ( $tst_i >= 0 ),
            'eth_getTransactionCount at block ' ~ $_hex ~ ': ' ~ $tst_i,
        );
    }
    else {
        skip 'eth_getBalance: no blocks in chain', 1;
    }

    $tst_i = $obj.eth_getTransactionCount($accnt, 'latest');
    ok ( $tst_i >= 0 ), 'eth_getTransactionCount latest: ' ~ $tst_i;

    $tst_i = $obj.eth_getTransactionCount($accnt, 'earliest');
    ok ( $tst_i >= 0 ), 'eth_getTransactionCount earliest: ' ~ $tst_i;

    $tst_i = $obj.eth_getTransactionCount($accnt, 'pending');
    ok ( $tst_i >= 0 ), 'eth_getTransactionCount pending: ' ~ $tst_i;

    # curl -H "Content-Type: application/json" -X POST --data '{"jsonrpc":"2.0","method":"eth_getTransactionByHash","params":["0x6c88ed755019615667b6aeb6fa29ec254ee7f4b75f21f0f4b5a5be919ecb1e52"],"id":1}' http://localhost:8600
    if $transact !~~ m:i/^ <[0x]>+ $/ {
        ok(
            ($transact ~~ m:i/^ 0x<[0..9a..f]>**64 $/ ),
            'valid transaction hash ' ~ $transact
        );
        %tst_h = $obj.eth_getTransactionByHash( $transact );
        ok(
            %tst_h,
            'eth_getTransactionByHash block no.' ~
            :16( %tst_h<blockNumber> || '0x0' )  ~
            ' (block hash: ' ~ ( %tst_h<blockHash> || 'Nil' ) ~ q{)},
        );
        %tst_h = $obj.eth_getTransactionReceipt( $transact );
        ok(
            %tst_h,
            'eth_getTransactionReceipt from ' ~
            ( %tst_h<from> || 'undefined' )   ~
            ' (status: ' ~ ( %tst_h<status> || '-1' ) ~
            ', used gas: ' ~ :16( %tst_h<gasUsed> || '0x0' ) ~ q{)},
        );
        if :16(%tst_h<status>) == 1 {
            $contract = $obj.retrieve_contract( $transact );
            ok(
                ($contract ~~ m:i/^ 0x<[0..9a..f]>**40 $/ ),
                'valid smart contract addr ' ~ $contract
            );
        }
        else {
            skip 'tx ' ~ $transact  ~ ' not mined', 1;
        }
    } else {
        skip 'no valid hash for eth_getTransactionByHash is passed through input args', 4;
    }

    # curl -H "Content-Type: application/json" -X POST --data '{"jsonrpc":"2.0","method":"eth_getBlockTransactionCountByHash","params":["0x49cddd11d530b9d09c637149eeb92e0da3a063964e125773c849722c3ce6ad1b"],"id":1}' http://localhost:8600

    $tst_i = $obj.eth_getBlockTransactionCountByHash( ( %tst_h<blockHash> || ( '0x' ~ '0' x 64 ) ).Str );
    ok ( $tst_i >= 0 ), 'eth_getBlockTransactionCountByHash: ' ~ $tst_i;

    $tst_i = $obj.eth_getBlockTransactionCountByHash( '0x49cddd11d530b9d09c637149eeb92e0da3a063964e125773c849722c3ce6ad10' );
    ok ( $tst_i >= 0 ), 'eth_getBlockTransactionCountByHash (invalid block hash): ' ~ $tst_i;

    $tst_r = $obj.wei2ether( 0 );
    is $tst_r, 0, 'wei2ether: 0 wei';

    $tst_r = $obj.wei2ether( 1 );
    is $tst_r, 0.000000000000000001, 'wei2ether: 1 wei';

    $tst_r = $obj.wei2ether( 2000 );
    is $tst_r, 0.000000000000002, 'wei2ether: 20000 wei';

    $tst_r = $obj.wei2ether( 883928732033678000000000 );
    is $tst_r, 883928.732033678, 'wei2ether: 883928732033678000000000 wei';

    # curl -H "Content-Type: application/json" -X POST --data '{"jsonrpc":"2.0","method":"eth_getCode","params":["0xb3abfa488058dba76206071b3600ae6e0dec3205","0x1e"],"id":1}' http://localhost:8600

    if $contract !~~ m:i/^ <[0x]>+ $/ {
        $block = $obj.eth_blockNumber;
        if $block > 0 {
            my Str $_hex = '0x' ~ $block.base(16).Str;
            # ($_hex ~ '=' ~ $tst_i).say;
            $tst_s = $obj.eth_getCode( $contract, $_hex );
            ok $tst_s, 'eth_getCode data (block ' ~ $_hex ~ '): ' ~ ( $tst_s.chars - 2 ) ~ ' bytes';
        } else {
            skip 'no blocks in chain', 1;
        }

        $tst_s = $obj.eth_getCode( $contract,'earliest' );
        ok $tst_s, 'eth_getCode data (earliest): ' ~ ( $tst_s.chars - 2 ) ~ ' bytes';

        $tst_s = $obj.eth_getCode( $contract,'latest' );
        ok $tst_s, 'eth_getCode data (latest): ' ~ ( $tst_s.chars - 2 ) ~ ' bytes';

        $tst_s = $obj.eth_getCode( $contract,'pending' );
        ok $tst_s, 'eth_getCode data (pending): ' ~ ( $tst_s.chars - 2 ) ~ ' bytes';

        # curl -H "Content-Type: application/json" -X POST --data '{"jsonrpc":"2.0","method":"eth_getTransactionReceipt","params":["0x6c88ed755019615667b6aeb6fa29ec254ee7f4b75f21f0f4b5a5be919ecb1e52"],"id":1}' http://localhost:8600

        %params = to => $contract, data => '0x30636152';
        $tst_s = $obj.eth_call( %params );

        my $max_id = ($tst_s ne '0x') ?? :16( $tst_s ) !! 0;
        my $id_to_remove = ( $max_id - 3 ) > 0 ?? ( $max_id - 3 ) !! $max_id;
        my $id_to_remove_data = sprintf("%064x", $id_to_remove);

        ok $tst_s ~~ m:i/ 0x<[\da..f]>* /, 'eth_call on get_max_id(): ' ~ $max_id;

        %params = to => $contract, data => '0xe7278e7f';
        $tst_s = $obj.eth_call( %params );
        my $rowcnt = ($tst_s ne '0x') ?? :16( $tst_s ) !! 0;
        ok $tst_s ~~ m:i/ 0x<[\da..f]>* /, 'eth_call on get_count(): ' ~ $rowcnt;

        %params = to => $contract, data => '0x173b35000000000000000000000000000000000000000000000000000000000000000002';
        $tst_s = $obj.eth_call( %params );
        ok $tst_s ~~ m:i/ 0x<[\da..f]>* /, 'eth_call on get_by_id(): ' ~ ( $tst_s.chars - 2 ) ~ ' bytes';

        if ( $obj.web3_clientVersion ~~ m:i/ 'parity-ethereum' /  ) {
            skip 'eth_call on 0x000001 on Parity-Ethereum', 1;
        } else {
            %params = to => $contract, data => '0x000001';
            $tst_s = $obj.eth_call( %params );
            ok $tst_s ~~ m:i/ 0x<[\da..f]>* /, 'eth_call on 0x000001: ' ~ $tst_s;
        }

        %params = to => $contract, data => '0x30636152';
        $tst_i = $obj.eth_estimateGas( %params );
        ok $tst_i > 0 , 'eth_estimateGas for get_max_id(): ' ~ $tst_i;

        %params = to => $contract, data => '0xe7278e7f';
        $tst_i = $obj.eth_estimateGas( %params );
        ok $tst_i > 0, 'eth_estimateGas for get_count(): ' ~ $tst_i;

        %params = to => $contract, data => '0x173b35000000000000000000000000000000000000000000000000000000000000000002';
        $tst_i = $obj.eth_estimateGas( %params );
        ok $tst_i > 0, 'eth_estimateGas for get_by_id(): ' ~ $tst_i;

        %params = from => $accnt, to => $contract, gas => sprintf("0x%x", 4700000), data => '0x00200170';
        $tst_s = $obj.eth_sendTransaction( %params );
        ok $tst_s, 'eth_sendTransaction init_db() hash: ' ~ $tst_s;

        %params = from => $accnt, to => $contract, gas => sprintf("0x%x", 4700000), data => '0x5ae9d5da' ~ $id_to_remove_data.Str;
        $tst_s = $obj.eth_sendTransaction( %params );
        ok $tst_s, 'eth_sendTransaction remove_by_id() hash: ' ~ $tst_s;

        $tst_i = $obj.eth_blockNumber;
        ok ( $tst_i > 0 ), 'eth_blockNumber: ' ~ $tst_i;
    } else {
        skip 'no valid smart contract hash is defined', 14;
    }
}

done-testing;
