unit class Net::Ethereum;

use HTTP::UserAgent;
use HTTP::Request;
use JSON::Fast;

has Str  $.api_url is rw;
has Str  $.abi is rw;
has Str  $.contract_id is rw;
has UInt $.tx_wait_sec is default(5) is rw;
has Bool $.keepalive   is default(False) is rw;
has Str  $.solidity        = '/usr/local/bin/solc';
has Str  $.conn_name       = 'parity-local';
has Bool $.debug           = False;
has Int  $.return_raw_data = 0;
has Bool $.show_progress   = False;
has Bool $.cleanup         = True;
has Str  $.unlockpwd;
has Any  $!ua = HTTP::UserAgent.new;

method check_ua_keepalive returns Bool {
    return (
            $!ua.^find_method('check_connection').defined &&
            $!ua.^find_method('store_connection').defined &&
            $!ua.^find_method('close_connection').defined
    );
}

method finalize_request returns Bool {
    ( self.check_ua_keepalive && $!keepalive ) ??
        $!ua.close_connection( name => $!conn_name ) !!
            False;
}

method !prepare_request( Hash $query ) returns HTTP::Request {
    my $request = HTTP::Request.new( POST => $!api_url );
    $request.header.field(
        Content-Type => 'application/json',
        Connection   => $!keepalive ?? 'Keep-Alive' !! 'close',
    );
    $request.add-content( to-json($query) );
    # ( '***DEBUG > request: ' ~ $request.perl ).say if $!debug;
    # ( '***DEBUG >  ' ~ "\n" ~ $request).say if $!debug;
    $request;
}

method !node_request( Hash $query ) returns Hash {
    my %ret;
    my $response;
    my $req = self!prepare_request($query);
    if $!keepalive {
        if ( self.check_ua_keepalive ) {
            if !( $!ua.check_connection( name => $!conn_name ) ) {
                $!ua.store_connection(
                    name     => $!conn_name,
                    conn     => $!ua.get-connection($req),
                );
            }
            $response = $!ua.request( $req, conn_name => $!conn_name );
        }
        else {
            # ( '***DEBUG > no keep-alive at HTTP::UserAgent' ).say if $!debug;
            $!keepalive = False;
            $response   = $!ua.request( $req  );
        }
    }
    else {
        $response = $!ua.request( $req  );
    }

    # ( '***DEBUG > response status: ' ~ $response.code ).say if $!debug;

    if $response.code == 200 {
        %ret = from-json( $response.content );
        if !(%ret<error>:exists) && !(%ret<result>:exists) {
            ( '***DEBUG > %ret: ' ~ %ret.gist ).say if $!debug;
            die 'Net::Ethereum invalid response content: ' ~ $response.content;
        }
        else {
            if %ret<error> {
                die %ret.gist;
            }
        }
    }
    else {
        die 'Net::Ethereum invalid response status: ' ~ $response.status-line;
    }
    %ret;
}

method !check_block( $block ) returns Str {
    my Str $ret;
    if $block ~~ UInt {
        $ret = ( '0x' ~ ( $block.base(16) // 0 ) );
    } elsif ( $block ~~ Str ) {
        if ( $block eq 'latest' || $block eq 'earliest' || $block eq 'pending' ) {
            $ret = $block;
        }
        else {
            die 'Argument $block should be one of latest/earliest/pending';
        }
    } else {
        die 'Argument $block is not type of UInt or Str';
    }
    $ret;
}

method node_ping returns Hash {
    my %rc      = retcode => 0, retmess => "success", rethash => Nil;
    my $rq      = %(
        jsonrpc => "2.0",
        method  => "web3_clientVersion",
        params  => [],
        id      => 67
    );
    try {
        %rc<rethash> = self!node_request($rq)<result>;
        CATCH {
            default {
                %rc<retcode> = -1;
                %rc<retmess> =
                    "Ethereum node is down - " ~ ( .Str ) ~ ". " ~
                    "Source: " ~ &?ROUTINE.name ~ " at line "  ~
                    .backtrace[1].line;
            }
        }
    }
    %rc;
}

method web3_clientVersion returns Str {
    my $rq = %( jsonrpc => "2.0", method => "web3_clientVersion", params => [], id => 67 );
    self!node_request($rq)<result>;
}

method web3_sha3( Str $val ) returns Str {
    my $rq = %( jsonrpc => "2.0", method => "web3_sha3", params => [ $val ], id => 64 );
    self!node_request($rq)<result>;
}

method net_version returns Int {
    my $rq = %( jsonrpc => "2.0", method => "net_version", params => [], id => 67 );
    self!node_request($rq)<result>.Int;
}

method net_listening returns Int {
    my $rq = %( jsonrpc => "2.0", method => "net_listening", params => [], id => 67 );
    self!node_request($rq)<result> ?? 1 !! 0;
}

method net_peerCount returns Int {
    my $rq = %( jsonrpc => "2.0", method => "net_peerCount", params => [], id => 74 );
    self!node_request($rq)<result>.Int;
}

method eth_protocolVersion returns Int {
    my $rq = %( jsonrpc => "2.0", method => "eth_protocolVersion", params => [], id => 67 );
    self!node_request($rq)<result>.Int;
}

method eth_syncing returns Bool {
    my $rq = %( jsonrpc => "2.0", method => "eth_syncing", params => [], id => 1 );
    my Int $rc = self!node_request($rq)<result>.Int;
    ( !$rc ?? False !! True )
}

method eth_coinbase returns Str {
    my $rq = %( jsonrpc => "2.0", method => "eth_coinbase", params => [], id => 64 );
    self!node_request($rq)<result>;
}

method eth_mining returns Int {
    my $rq = %( jsonrpc => "2.0", method => "eth_mining", params => [], id => 71 );
    self!node_request($rq)<result> ?? 1 !! 0;
}

method eth_hashrate returns Int {
    my $rq = %( jsonrpc => "2.0", method => "eth_hashrate", params => [], id => 71 );
    self!node_request($rq)<result>.Int;
}

method eth_gasPrice returns Int {
    my $rq = %( jsonrpc => "2.0", method => "eth_gasPrice", params => [], id => 73 );
    self!node_request($rq)<result>.Int;
}

method eth_accounts returns Array {
    my $rq = %( jsonrpc => "2.0",method => "eth_accounts", params => [], id => 1 );
    self!node_request($rq)<result>.Array;
}

method eth_blockNumber returns Int {
    my $rq = %( jsonrpc => "2.0", method => "eth_blockNumber", params => [], id => 83 );
    self!node_request($rq)<result>.Int;
}

method personal_unlockAccount( Str :$account?, Str :$password? ) returns Bool {
    my $rq = %(
        jsonrpc => "2.0",
        method  => "personal_unlockAccount",
        params  =>
            [
                ( $account  // self.eth_accounts[0] ),
                ( $password // $!unlockpwd )
            ],
        id      => 1,
    );
    self!node_request($rq)<result>.Bool;
}

method eth_getBalance( Str $account, Str $tag ) returns Int {
    if !( $tag eq 'latest' || $tag eq 'earliest' || $tag eq 'pending' ) {
        if !$tag || !( $tag ~~ m:i/ 0x<[\da..f]>* / ) || !( :16($tag) ~~ Int ) {
            die 'Net::Ethereum invalid tag at eth_getBalance: ' ~ $tag;
        }
    }
    my $rq = %( jsonrpc => "2.0", method => "eth_getBalance", params => [ $account, $tag ], id => 1 );
    self!node_request($rq)<result>.Int;
}

method eth_getTransactionCount( Str $account, Str $tag ) returns Int {
    if !( $tag eq 'latest' || $tag eq 'earliest' || $tag eq 'pending' ) {
        if !$tag || !( $tag ~~ m:i/ 0x<[\da..f]>* / ) || !( :16($tag) ~~ Int ) {
            die 'Net::Ethereum invalid tag at eth_getTransactionCount: ' ~ $tag;
        }
    }
    my $rq = %( jsonrpc => "2.0", method => "eth_getTransactionCount", params => [ $account, $tag ], id => 1 );
    self!node_request($rq)<result>.Int;
}

method eth_getTransactionByHash( Str $hash ) returns Hash {
    my $rq = { jsonrpc => "2.0", method => "eth_getTransactionByHash", params => [ ~($hash) ] , id => 1};
    self!node_request($rq)<result> || %( Nil );
}

method eth_getBlockByNumber( $block, Bool $full_tx_obj? ) returns Hash {
    my %ret;
    my Str $block_param = self!check_block( $block );
    my $rq = {
        jsonrpc => "2.0",
        method  => "eth_getBlockByNumber",
        params  => [
            $block_param,
            $full_tx_obj // False
        ],
        id => 1
    };
    self!node_request($rq)<result> || %( Nil );
}

method eth_getBlockByHash( Str $blockhash, Bool $full_tx_obj? ) returns Hash {
    if !($blockhash ~~ m:i/^ 0x<[\da..f]>**64 $/) {
        die 'Invalid block hash length: not 32 bytes';
    }
    my $rq = {
        jsonrpc => "2.0",
        method  => "eth_getBlockByHash",
        params  => [
            $blockhash,
            $full_tx_obj // False
        ],
        id => 1
    };
    self!node_request($rq)<result> || %( Nil );
}

method eth_getBlockTransactionCountByHash( Str $blockhash ) returns Int {
    my $rq  = %( jsonrpc => "2.0", method => "eth_getBlockTransactionCountByHash", params => [ ~($blockhash) ], id => 1 );
    my $cnt = self!node_request($rq)<result>;
    ( $cnt ?? $cnt.Int !! 0 );
}

method eth_getCode( Str $addr, Str $tag ) returns Str {
    if !( $tag eq 'latest' || $tag eq 'earliest' || $tag eq 'pending' ) {
        if !( $tag ~~ m:i/ 0x<[\da..f]>* / ) || !( :16($tag) ~~ Int ) {
            die 'Net::Ethereum invalid tag at eth_getCode: ' ~ $tag;
        }
    }
    my $rq = %(
        jsonrpc => "2.0",
        method  => "eth_getCode",
        params => [ $addr, $tag ], id => 1
    );
    self!node_request($rq)<result>.Str;
}

method wei2ether( UInt $wei ) returns Real {
  my $wei_in_ether = 1000000000000000000;
  ( $wei / $wei_in_ether );
}

method eth_getTransactionReceipt( Str $hash ) returns Hash {
    my $rq = %( jsonrpc => "2.0", method => "eth_getTransactionReceipt", params => [ ~($hash) ], id => 1 );
    self!node_request($rq)<result> || %( Nil );
}

method eth_call( %params ) returns Str {
    my $rq = %( jsonrpc => "2.0", method => "eth_call", params => [ { to => %params<to>, data => %params<data> }, "latest", ], id => 1 );
    self!node_request($rq)<result>.Str;
}

method eth_estimateGas( %params ) returns Int {
    my $rq = %(
        jsonrpc => "2.0",
        method  => "eth_estimateGas",
        params  => [ { to => %params<to>, data => %params<data> }, ],
        id      => 1
    );
    self!node_request($rq)<result>.Int;
}

method eth_sendTransaction( %params ) returns Str {
    my $rq = %(
        jsonrpc => "2.0",
        method => "eth_sendTransaction",
        params => [
            {
                from => %params<from>,
                to   => %params<to>,
                gas  => %params<gas>,
                data => %params<data>
            },
        ],
        id => 1
    );
    my %h = self!node_request($rq);

    ( '***DEBUG > tx: ' ~ %h<result>.Str ).say if $!debug;
    %h<result>.Str;
}

method pack_filter_params(
    :$fromblock?,
    :$toblock?,
    Str :$address?,
    :@topics
) returns Hash {
    my %ret =
        fromBlock => 'earliest',
        toBlock   => 'latest',
        topics    => []
    ;
    %ret<fromBlock> = self!check_block( $fromblock ) if $fromblock;
    %ret<toBlock>   = self!check_block( $toblock )   if $toblock;
    %ret<address>   =
        $address if $address.defined && $address ~~ m:i/^ 0x<[\da..f]>**64 $/;
    if ( @topics.elems == 1 && @topics[0] ~~ m:i/^ 0x<[\da..f]>**64 $/ ) {
        %ret<topics> = @topics
    }
    %ret;
}

method eth_getLogs( $filter ) returns List {
    my $rq = %(
        jsonrpc => "2.0",
        method => "eth_getLogs",
        params => [ $filter ],
        id => 74
    );
    self!node_request($rq)<result> || Empty;
}

method hex2string( Str $hex ) returns Str {
    my @reencoded;
    my @codes = ( $hex ~~ m:g/../ ).map({ :16($_.Str) if $_ ne '0x' });
    my $blob = Blob.new( @codes );
    $blob.decode;
}

method string2hex( Str $s ) returns Str {
    my @reencoded;
    my @encoded = $s.encode.map({ $_ });
    for @encoded -> $data {
        my $hex = $data.base(16);
        @reencoded.push( ( $data < 16 ) ?? '0' ~ $hex !! $hex );
    }
    '0x' ~ @reencoded.join(q{});
}

method getContractMethodId( Str $method_name ) returns Str {
    my $method_name_hex = self.string2hex($method_name);
    my $hash = self.web3_sha3($method_name_hex);
    substr( $hash, 0, 10 );
}

method get_function_abi( Str $func ) returns Hash {
    my %ret;
    my $abi = from-json( $!abi || die '$!abi attribute is not set' );
    # ( '***DEBUG > abi: ' ~ $abi.gist ).say if $!debug;
    for ( @$abi ) -> %_ret {
        if %_ret<type> eq 'function' && %_ret<name> eq $func {
            %ret = %_ret;
            last;
        }
    }
    %ret;
}

method get_constructor_abi returns Hash {
    my %ret;
    my $abi = from-json( $!abi );
    # ( '***DEBUG > abi: ' ~ $abi.gist ).say if $!debug;
    for ( @$abi ) -> %_ret {
        if %_ret<type> eq 'constructor' {
            %ret = %_ret;
            last;
        }
    }
    %ret;
}

method marshal_int( Int $marshal ) returns Str {
    my Int $_in = $marshal;
    my Int $filler_size;
    my Str $bint_hex;
    my Str $filler_char;
    if $_in < 0 {
        $_in .= abs;
        $_in +^= 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
        $_in++;
        $filler_char = 'f';
    }
    else {
        $filler_char = '0';
    }
    $bint_hex = $_in.base(16).Str;
    $filler_size = 64 - $bint_hex.chars;
    return ( $filler_char x $filler_size ) ~ $bint_hex.lc;
}

method unmarshal_int( Str $unmarshal ) returns Int {
    my $u_int = :16($unmarshal);
    my $neg = :16($unmarshal);
    if ( $neg +& 0x8000000000000000000000000000000000000000000000000000000000000000 ) {
        $u_int +^= 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
        $u_int++;
        $u_int *= -1;
    }
    $u_int;
}

method marshal(Str $fname, Hash $fparams) returns Str {
    my %fabi;
    if $fname eq 'constructor' {
        %fabi = self.get_constructor_abi;
    }
    else {
        %fabi = self.get_function_abi( $fname );
    }

    my $inputs = %fabi<inputs>;
    my Int $current_out_param_position = 0;
    my Int $out_param_array_counter    = 0;
    my Str $param_types_list;
    my Str $encoded_arguments;
    my Str $add_hunk;
    my Str $raw;
    my @out_param_array;

    # First pass
    for @$inputs -> %out_param {
        my Str $cur_pname  = %out_param<name>;
        my Str $cur_ptype  = %out_param<type>;
        $param_types_list ~= $cur_ptype ~ q{,};

        if $cur_ptype eq 'bool' || $cur_ptype ~~ m:i/^ u?int / {
            $add_hunk=self.marshal_int( $fparams{$cur_pname} );
            @out_param_array[$out_param_array_counter] = $add_hunk;
        }
        elsif $cur_ptype eq 'address' {
            $add_hunk = sprintf( "%064s", substr($fparams{$cur_pname}, 2) );
            @out_param_array[$out_param_array_counter] = $add_hunk;
        }
        elsif $cur_ptype eq 'string' {
            $current_out_param_position += 64;
            @out_param_array[$out_param_array_counter] = $fparams{$cur_pname}.chars; # replace with data hunk offset into second pass
        }
        else {
            die 'Net::Ethereum marshal does not support type: ' ~ $cur_ptype;
        }
        $out_param_array_counter++;
        $current_out_param_position += 64;
    }

    ( '***DEBUG > : {$out_param_array_counter=' ~ $out_param_array_counter ~ q{\}} ~ "\n" ~ @out_param_array.join("\n") ).say if $!debug;

    # Second pass
    my Int $var_data_offset_index = $out_param_array_counter;
    my Int $var_data_offset = $var_data_offset_index * 32;
    my Int $array_index         = 0;
    my Int $second_pass_counter = 0;

    for @$inputs -> %out_param {
        my Str $cur_pname  = %out_param<name>;
        my Str $cur_ptype  = %out_param<type>;
        if $cur_ptype eq 'address' || $cur_ptype eq 'bool' || $cur_ptype ~~ m:i/^ u?int / {
            $array_index++;
        }
        elsif $cur_ptype eq 'string' {
            my Str $_fname  = $fparams{$cur_pname};
            my Str $hunk    = self.string2hex( $_fname );
            my Int $hsize   = $hunk.chars;
            my Int $number_of_32b_blocks = ($hsize / 64).floor;
            my Int $part = $hsize % 64;
            my Str $repeater = '0' x ( 64 - $part + 2 );
            my Str $hunk_appended = substr( $hunk ~ $repeater, 2 );

            ( '***DEBUG > : ' ~
                "\$array_index=$array_index\n" ~
                "\$var_data_offset=$var_data_offset\n" ~
                "\$_fname=$_fname\n\$hunk=$hunk\n" ~
                "\$hsize=$hsize\n" ~
                "\$number_of_32b_blocks=$number_of_32b_blocks\n" ~
                "\$part=$part\n" ~
                "\$repeater=$repeater\n" ~
                "\$hunk_appended=$hunk_appended\n"
            ).say if $!debug;

            my @length =
                ( $hunk ~~ m:g/../ ).map({ $_ if $_ ne '0x' });

            @out_param_array[$array_index] = sprintf("%064x", $var_data_offset);
            $var_data_offset += ( ( $number_of_32b_blocks + 1 ) * 32 ) + 32;
            my $val = sprintf("%064x", @length.elems );
            ( '***DEBUG > length: ' ~ $val ).say if $!debug;
            @out_param_array[$var_data_offset_index + $second_pass_counter] = $val;
            $array_index++;
            $second_pass_counter++;

            my Int $hunk_position=0;
            while 1 {
                my Str $cur_str = substr( $hunk_appended, $hunk_position, 64 );
                if !$cur_str {
                    last;
                }
                @out_param_array[$var_data_offset_index + $second_pass_counter] = $cur_str;
                $second_pass_counter++;
                $hunk_position += 64;
            }
        }
    }
    if $fname eq 'constructor' {
        $raw = @out_param_array.join( q{} );
    }
    else {

        # Get Contract Method Id
        my $contract_method_id = self.getContractMethodId(
            $fname ~ q{(} ~ ( $param_types_list.chop if $param_types_list ) ~ q{)}
        );
        $raw = $contract_method_id ~ @out_param_array.join(q{});
        ( '***DEBUG > marshall array (method_id=' ~ ( $contract_method_id || 'undefined' )  ~ ')' ~ "\n" ~ @out_param_array.join( "\n" ) ).say if $!debug;
    }
    $raw;
}

method unmarshal( Str $fname, Str $raw_data ) returns Hash {
    my %fabi = self.get_function_abi( $fname );
    my $outputs = %fabi<outputs>;
    my %return_value;
    if $!return_raw_data {
        %return_value.push: ( raw_data => $raw_data );
    }

    my Int $current_out_param_position = 0;
    my Int $index_hash_key             = 0;
    ( '***DEBUG > unmarshal outputs: ' ~ @$outputs.gist ).say if $!debug;
    for @$outputs -> %out_param {
        my Str $cur_pname = %out_param<name> || $index_hash_key++;
        my Str $cur_ptype = %out_param<type>;

        if $cur_ptype eq 'bool' || $cur_ptype ~~ m:i/^ u?int / {
            my Str $hunk = '0x' ~ substr( $raw_data, $current_out_param_position, 64 );
            my $_udata   = self.unmarshal_int($hunk);
            %return_value.push: ( $cur_pname => $_udata );
        }
        elsif $cur_ptype eq 'address' {
            my Str $hunk = '0x' ~ substr( $raw_data, $current_out_param_position, 64 );
            $hunk ~~ s:Perl5:g/00//;
            %return_value.push: ( $cur_pname => $hunk );
        }
        elsif $cur_ptype eq 'string' {
            my Int $size_offset = :16( substr($raw_data, $current_out_param_position, 64) );
            my Int $data_size   = :16( substr($raw_data, $size_offset * 2, 64) );
            my Str $data_chunk  = substr( $raw_data, 64 + $size_offset*2, $data_size * 2 );
            %return_value.push: ( $cur_pname => self.hex2string('0x' ~ $data_chunk) );
            my $str = self.hex2string('0x' ~ $data_chunk);
        }
        else {
            die 'Net::Ethereum _marshal does not support type: ' ~ $cur_ptype;
        }
        $current_out_param_position += 64;
    }
    %return_value;
}

method sendTransaction( Str $src_account, Str $contract_id, Str $fname, Hash $fparams, Int $gas) returns Str {
    my $raw = self.marshal($fname, $fparams);
    my %par =
        from => $src_account,
        to   => $contract_id,
        data => $raw,
        gas  => '0x' ~ $gas.base(16),
    ;
    self.eth_sendTransaction( %par );
}

method deploy_contract_estimate_gas( Str $src_account, Str $contract_binary, Hash $constructor_params ) returns Int {
    my %par = from => $src_account;
    if $constructor_params {
        my $raw = self.marshal( 'constructor', $constructor_params );
        %par.push: ( data => $contract_binary ~ $raw );
    }
    else {
        %par.push: ( data => $contract_binary );
    }
    self.eth_estimateGas( %par );
}

method deploy_contract( Str $src_account, Str $contract_binary, Hash $constructor_params, Int $gas ) returns Str {
    my %par = from => $src_account;
    if $constructor_params {
        my $raw = self.marshal( 'constructor', $constructor_params );
        %par.push: ( data => $contract_binary ~ $raw );
    }
    else {
        %par.push: ( data => $contract_binary );
    }
    %par.push: ( gas => '0x' ~ $gas.base(16) );
    self.eth_sendTransaction( %par );
}

method wait_for_transaction( :@hashes!, Int :$iters?, Bool :$contract? ) returns Array {
    my Str $new_contract_id;
    my @_hashes = @hashes;
    my @rc;
    my %stat;
    for (0..( $iters // 10 ) ) {
        for @_hashes.kv -> $indx, $hash {
#           "$_: $indx-$hash".say;
            if (@_hashes[$indx]:exists) {
                my %rc = self.eth_getTransactionByHash( $hash );
                if %rc<blockNumber> {
                    %stat = self.eth_getTransactionReceipt( $hash );
                    if :16(%stat<status>) == 1  {
                        if $contract {
                            $new_contract_id = %stat<contractAddress>;
                            $!contract_id = $new_contract_id;
                        }
                        @rc.push(%stat);
                    }
                    @_hashes[$indx]:delete;
                }
            }
        }
        if @rc.elems == @hashes.elems {
            if $!show_progress {
                "\n".print;
            }
            last;
        } else {
            sleep( $!tx_wait_sec );
            if $!show_progress {
                ( '.' ~ $_ ~ '(' ~ @rc.elems ~ '/' ~ @hashes.elems ~ ')' ).print;
            }
        }
    }
#   @rc.gist.say;
    @rc;
}

method contract_method_call_estimate_gas( Str $fname, Hash $fparams ) returns Int {
    my $raw = self.marshal( $fname, $fparams);
    my %par = to => ( $!contract_id || die '$!contract_id attribute is not set' ), data => $raw;
    self.eth_estimateGas( %par );
}

method contract_method_call( Str $fname, Hash $fparams ) returns Hash {
    my $raw = self.marshal( $fname, $fparams);
    my %par = to => ( $!contract_id || die '$!contract_id attribute is not set' ), data => $raw;
    if $!debug {
        ( 'Function name: '       ~ $fname ).say;
        ( 'Function params: '     ~ $fparams.gist ).say;
        ( 'Function raw params: ' ~ $raw ).say;
    }

    my $rc = self.eth_call( %par );
    if $!debug {
        ( 'eth_call return data: ' ~ $rc ).say;
    }
    $raw = substr($rc, 2);
    self.unmarshal( $fname, $raw );
}

method compile_and_deploy_contract(
    Str  :$contract_src_path,
    Str  :$compile_output_path,
    Hash :$constructor_params?,
    Str  :$src_account?,
    Str  :$password?
) returns Hash {
    my Str $acc = $src_account // self.eth_accounts[0];
    my Str $cmd = $!solidity ~ ' --bin --abi ' ~ $contract_src_path ~ ' -o ' ~ $compile_output_path  ~ ' --overwrite';
    my Str $contract_name = ( ( $contract_src_path.IO.basename ).IO.extension: '' ).Str;
    ( '***DEBUG > solidity path: ' ~ $!solidity ~ ', command: ' ~ $cmd ~ ', contract name: ' ~ $contract_name ).say if $!debug;
    if !$compile_output_path.IO.e || !$!solidity.IO.e || !shell( $cmd ) {
        die 'Failed to compile ' ~ $contract_name ~ ' with command: ' ~ $cmd;
    }
    my Str $bin = '0x' ~ ( $compile_output_path ~ '/' ~ $contract_name ~ '.bin' ).IO.slurp;
    $!abi       = ( $compile_output_path ~ '/' ~ $contract_name ~ '.abi' ).IO.slurp;

    self.personal_unlockAccount(
        account  => $src_account,
        password => $password,
    );

    my $contract_used_gas     = self.deploy_contract_estimate_gas( $acc, $bin, $constructor_params );
    my $gas_price             = self.eth_gasPrice;
    my $contract_deploy_price = $contract_used_gas * $gas_price;
    my $price_in_eth          = self.wei2ether( $contract_deploy_price );

    ( '***DEBUG > contract gas estimated: ' ~ $contract_used_gas ~ ' wei (0x' ~ $contract_used_gas.base(16) ~ ' wei), $price_in_eth: ' ~ $price_in_eth ~ ' ether', "\n" ).say if $!debug;

    my $contrarc_deploy_tr    = self.deploy_contract($acc, $bin, $constructor_params, $contract_used_gas);
    my %contract_status       = ( self.wait_for_transaction( :hashes( @($contrarc_deploy_tr) ), :contract( True ) ) )[0];
    my $contract_code         = self.eth_getCode( %contract_status<contractAddress>, "latest" );
    if $contract_code eq '0x' {
        die 'Error: no contract code from network for ' ~ $contrarc_deploy_tr ~ '(' ~ $contract_code ~ ')';
    }
    if $!cleanup {
        for $compile_output_path.IO.dir( test => /:i <{$contract_name}> '.' <[a..z]>+ $/ ) -> $file {
            if !$file.unlink {
                ( '***DEBUG > could not unlink at cleanup with ' ~ $file.basename ).say if $!debug;
            }
        }
    }
    %contract_status || %( Nil );
}

method retrieve_contract( Str $hash ) returns Str {
   my $rq = %( jsonrpc => "2.0", method => "eth_getTransactionReceipt", params => [ ~($hash) ], id => 1 );
   self!node_request($rq)<result><contractAddress> || "0x" ~ q{0} x 40;
}
